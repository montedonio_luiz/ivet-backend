require('dotenv/config');

module.exports = {
  url: process.env.NODE_ENV === 'production' ? process.env.DATABASE_URL : process.env.DB_URL_DEV,
  dialect: 'postgres',
  define: {
    underscored: true
  }
}
