// Dependencies
const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const messages = require('../utils/messages');
const authConfig = require('../../config/auth');

module.exports = {
  async authenticate(req, res, next) {
    const authHeader = req.headers.authorization

    if (!authHeader) {
      return res.status(401).json({ error: messages.error.token.missing })
    }

    const [, token] = authHeader.split(' ')

    try {
      const decoded = await promisify(jwt.verify)(token, authConfig.secret)
      
      req.user = {
        id: decoded.id,
      }

      return next()
    } catch (err) {
      return res.status(401).json({ error: messages.error.token.invalid })
    }
  },
}