const Frequencia = require('../models/Frequencia');

// Validation
const FrequenciaValidation = require('../validation/Frequencia');

// Dependencies
const messages = require('../utils/messages');

class FrequenciaController {

  /**
   * Creates a Frequencia
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new Frequencia. 
  */
  async create(req, res) {
    let validation = await FrequenciaValidation.create(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let frequencia = await Frequencia.create(req.body).then(resp => Frequencia.findOne({ where: { id: resp.id } }));

    return res.status(201).json(frequencia);
  }

  /**
  * Return all Frequencias
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all Frequencias. 
  */
  async readAll(req, res) {
    let frequencias = await Frequencia.findAll();

    return res.status(200).json(frequencias);
  }

  /**
  * Return a specific Frequencia
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns a specififc Frequencia. 
  */
  async readOne(req, res) {
    let frequencia = await Frequencia.findOne({ where: { id: req.params.id } });

    return res.status(200).json(frequencia);
  }

  /**
  * Update an Frequencia
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns the updated Frequencia. 
  */
  async update(req, res) {
    let oldFrequencia = await Frequencia.findOne({
      where: {
        id: req.params.id
      }
    })

    if (oldFrequencia) {
      let validation = await FrequenciaValidation.update(req.params.id, req.body);
      if (!validation.valid) {
        delete validation.valid;
        return res.status(400).json(validation);
      }

      let newFrequencia = await oldFrequencia.update(req.body).then(resp => Frequencia.findOne({ where: { id: resp.id } }));

      return res.status(200).json(newFrequencia);
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }

  /**
   * Delete Frequencia
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns delete message. 
   */
  async delete(req, res) {
    let frequencia = await Frequencia.findOne({
      where: {
        id: req.params.id
      }
    });

    if (frequencia) {
      await frequencia.destroy();
      return res.status(200).json({ message: messages.success.deleted });
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }
}

module.exports = new FrequenciaController();