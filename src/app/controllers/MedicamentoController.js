const Medicamento = require('../models/Medicamento');

// Dependencies
const messages = require('../utils/messages');

// Validation
const MedicamentoValidation = require('../validation/Medicamento');

// Fields
const fields = ['id', 'nome', 'dose_max', 'dose_min', 'dose_unica', 'concentracao'];

// Relations
const Via = require('../models/Via');
const Especie = require('../models/Especie');
const Frequencia = require('../models/Frequencia');
const UnidadeMedida = require('../models/UnidadeMedida');

// Joins
const attributions = [
  {
    model: Especie,
    attributes: ['id', 'nome']
  },
  {
    model: UnidadeMedida,
    attributes: ['id', 'nome']
  },
  {
    model: Via,
    as: 'Vias',
    attributes: ['id', 'nome'],
    through: {
      attributes: []
    },
  },
  {
    model: Frequencia,
    as: 'Frequencias',
    attributes: ['id', 'nome'],
    through: {
      attributes: []
    },
  },
];

// Options
const options = {
  attributes: fields,
  include: attributions
};


class MedicamentoController {

  /**
   * Creates a Medicamento
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new Medicamento. 
  */
  async create(req, res) {
    let validation = await MedicamentoValidation.create(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let medicamento = await Medicamento
      .create(req.body)
      .then(async medicamento => {
        await medicamento.setVias(req.body.Vias)
        return medicamento
      })
      .then(async medicamento => {
        if (req.body.Frequencias) {
          await medicamento.setFrequencias(req.body.Frequencias)
        }
        return medicamento
      })
      .then(medicamento => Medicamento.findOne({ ...options, where: { id: medicamento.id } }))

    return res.status(201).json(medicamento)
  }

  /**
  * Return all Medicamentos
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all Medicamentos. 
  */
  async readAll(req, res) {
    let medicamentos = await Medicamento.findAll(options);

    return res.status(200).json(medicamentos);
  }

  /**
  * Return a specific Medicamento
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns a specififc Medicamento. 
  */
  async readOne(req, res) {
    let medicamento = await Medicamento.findOne({ ...options, where: { id: req.params.id } });

    return res.status(200).json(medicamento);
  }

  /**
  * Update an Medicamento
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns the updated Medicamento. 
  */
  async update(req, res) {
    let oldMedicamento = await Medicamento.findOne({ where: { id: req.params.id } })

    if (oldMedicamento) {
      let validation = await MedicamentoValidation.update(req.params.id, req.body);
      if (!validation.valid) {
        delete validation.valid;
        return res.status(400).json(validation);
      }

      let newMedicamento = await oldMedicamento
        .update(req.body)
        .then(async medicamento => {
          await medicamento.setVias(req.body.Vias)
          return medicamento
        })
        .then(async medicamento => {
          if (req.body.Frequencias) {
            await medicamento.setFrequencias(req.body.Frequencias)
          }
          return medicamento
        })
        .then(medicamento => Medicamento.findOne({ ...options, where: { id: medicamento.id } }));


      return res.status(200).json(newMedicamento);
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }

  /**
   * Delete Medicamento
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns delete message. 
   */
  async delete(req, res) {
    let medicamento = await Medicamento.findOne({ where: { id: req.params.id } })

    if (medicamento) {
      await medicamento.destroy();
      return res.status(200).json({ message: messages.success.deleted });
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }
}

module.exports = new MedicamentoController();