const MarcaRacao = require('../models/MarcaRacao');

// Validation
const MarcaRacaoValidation = require('../validation/MarcaRacao');

// Dependencies
let messages = require('../utils/messages');

class MarcaRacaoController {

  /**
   * Creates a MarcaRacao
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new MarcaRacao. 
  */
  async create(req, res) {
    let validation = await MarcaRacaoValidation.create(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let marca_racao = await MarcaRacao.create({ nome: req.body.nome, }).then(resp => MarcaRacao.findOne({ where: { id: resp.id } }));


    return res.status(201).json(marca_racao);
  }

  /**
  * Return all MarcaRacao
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all MarcaRacaos. 
  */
  async readAll(req, res) {
    let marcaRacaos = await MarcaRacao.findAll();

    return res.status(200).json(marcaRacaos);
  }

  /**
  * Return a specific MarcaRacao
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns a specififc MarcaRacao. 
  */
  async readOne(req, res) {
    let marcaRacao = await MarcaRacao.findOne({
      where: {
        id: req.params.id
      }
    });

    return res.status(200).json(marcaRacao);
  }

  /**
  * Update a MarcaRacao
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns the updated MarcaRacao. 
  */
  async update(req, res) {
    let oldMarcaRacao = await MarcaRacao.findOne({
      where: {
        id: req.params.id
      }
    })

    if (oldMarcaRacao) {
      let validation = await MarcaRacaoValidation.update(req.params.id, req.body);
      if (!validation.valid) {
        delete validation.valid;
        return res.status(400).json(validation);
      }

      let newMarcaRacao = await oldMarcaRacao.update(req.body).then(resp => MarcaRacao.findOne({ where: { id: resp.id } }));

      return res.status(200).json(newMarcaRacao);
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }

  /**
   * Delete MarcaRacao
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns delete message. 
   */
  async delete(req, res) {
    let marcaRacao = await MarcaRacao.findOne({
      where: {
        id: req.params.id
      }
    });

    if (marcaRacao) {
      await marcaRacao.destroy();
      return res.status(200).json({ message: messages.success.deleted });
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }
}

module.exports = new MarcaRacaoController();