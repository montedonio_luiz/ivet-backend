const Via = require('../models/Via');

// Validation
const ViaValidation = require('../validation/Via');

// Dependencies
const messages = require('../utils/messages');

class ViaController {

  /**
   * Creates a Via
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new Via. 
  */
  async create(req, res) {
    let validation = await ViaValidation.create(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let via = await Via.create(req.body).then(resp => Via.findOne({ where: { id: resp.id } }));

    return res.status(201).json(via);
  }

  /**
  * Return all Vias
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all Vias. 
  */
  async readAll(req, res) {
    let vias = await Via.findAll();

    return res.status(200).json(vias);
  }

  /**
  * Return a specific Via
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns a specififc Via. 
  */
  async readOne(req, res) {
    let via = await Via.findOne({ where: { id: req.params.id } });

    return res.status(200).json(via);
  }

  /**
  * Update an Via
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns the updated Via. 
  */
  async update(req, res) {
    let oldVia = await Via.findOne({
      where: {
        id: req.params.id
      }
    })

    if (oldVia) {
      let validation = await ViaValidation.update(req.params.id, req.body);
      if (!validation.valid) {
        delete validation.valid;
        return res.status(400).json(validation);
      }

      let newVia = await oldVia.update(req.body).then(resp => Via.findOne({ where: { id: resp.id } }));

      return res.status(200).json(newVia);
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }

  /**
   * Delete Via
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns delete message. 
   */
  async delete(req, res) {
    let via = await Via.findOne({
      where: {
        id: req.params.id } });

    if (via) {
      await via.destroy();
      return res.status(200).json({ message: messages.success.deleted });
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }
}

module.exports = new ViaController();