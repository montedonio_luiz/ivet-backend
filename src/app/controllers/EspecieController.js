const Especie = require('../models/Especie');

// Validation
const EspecieValidation = require('../validation/Especie');

// Dependencies
const messages = require('../utils/messages');


class EspecieController {

  /**
   * Creates a Especie
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new Especie. 
  */
  async create(req, res) {
    let validation = await EspecieValidation.create(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let especie = await Especie.create(req.body).then(resp => Especie.findOne({ where: { id: resp.id } }));

    return res.status(201).json(especie);
  }

  /**
  * Return all Especies
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all Especies. 
  */
  async readAll(req, res) {
    let especies = await Especie.findAll();

    return res.status(200).json(especies);
  }

  /**
  * Return a specific Especie
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns a specififc Especie. 
  */
  async readOne(req, res) {
    let especie = await Especie.findOne({ where: { id: req.params.id } });

    return res.status(200).json(especie);
  }

  /**
  * Update an Especie
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns the updated Especie. 
  */
  async update(req, res) {
    let oldEspecie = await Especie.findOne({
      where: {
        id: req.params.id
      }
    })

    if (oldEspecie) {
      let validation = await EspecieValidation.update(req.params.id, req.body);
      if (!validation.valid) {
        delete validation.valid;
        return res.status(400).json(validation);
      }

      let newEspecie = await oldEspecie.update(req.body).then(resp => Especie.findOne({ where: { id: resp.id } }));

      return res.status(200).json(newEspecie);
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }

  /**
   * Delete Especie
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns delete message. 
   */
  async delete(req, res) {
    let especie = await Especie.findOne({ where: { id: req.params.id } });

    if (especie) {
      await especie.destroy();
      return res.status(200).json({ message: messages.success.deleted });
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }
}

module.exports = new EspecieController();