const Racao = require('../models/Racao');

// Validation
const RacaoValidation = require('../validation/Racao');

// Dependencies
const messages = require('../utils/messages');

// Fields
const fields = ['id', 'nome', 'energia_metabolizavel'];

// Relations
const Especie = require('../models/Especie');
const MarcaRacao = require('../models/MarcaRacao');

// Joins
const attributions = [
  {
    model: Especie,
    attributes: ['id', 'nome']
  },
  {
    model: MarcaRacao,
    attributes: ['id', 'nome']
  },
];

// Options
const options = {
  attributes: fields,
  include: attributions
};


class RacaoController {

  /**
   * Creates a Racao
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new Racao. 
  */
  async create(req, res) {
    let validation = await RacaoValidation.create(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let racao = await Racao.create(req.body).then(resp => Racao.findOne({ ...options, where: { id: resp.id } }));

    return res.status(201).json(racao);
  }

  /**
  * Return all Racaos
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all Racaos. 
  */
  async readAll(req, res) {
    let racaos = await Racao.findAll(options);

    return res.status(200).json(racaos);
  }

  /**
  * Return a specific Racao
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns a specififc Racao. 
  */
  async readOne(req, res) {
    let racao = await Racao.findOne({ ...options, where: { id: req.params.id } });

    return res.status(200).json(racao);
  }

  /**
  * Update an Racao
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns the updated Racao. 
  */
  async update(req, res) {
    let oldRacao = await Racao.findOne({
      where: {
        id: req.params.id
      }
    })

    if (oldRacao) {
      let validation = await RacaoValidation.update(req.params.id, req.body);
      if (!validation.valid) {
        delete validation.valid;
        return res.status(400).json(validation);
      }

      let newRacao = await oldRacao.update(req.body).then(resp => Racao.findOne({ ...options, where: { id: resp.id } }));

      return res.status(200).json(newRacao);
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }

  /**
   * Delete Racao
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns delete message. 
   */
  async delete(req, res) {
    let racao = await Racao.findOne({
      where: {
        id: req.params.id
      }
    })

    if (racao) {
      await racao.destroy();
      return res.status(200).json({ message: messages.success.deleted });
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }
}

module.exports = new RacaoController();