const UnidadeMedida = require('../models/UnidadeMedida');


class UnidadeMedidaController {

  /**
  * Return all UnidadeMedidas
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all UnidadeMedidas. 
  */
  async readAll(req, res) {
    let unidadeMedidas = await UnidadeMedida.findAll();

    return res.status(200).json(unidadeMedidas);
  }
}

module.exports = new UnidadeMedidaController();