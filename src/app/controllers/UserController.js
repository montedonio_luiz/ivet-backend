const User = require('../models/User');

// Validation
const UserValidation = require('../validation/User');

// Dependencies
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Auth = require('../../config/auth');
const messages = require('../utils/messages');

class UserController {
  /**
   * Creates a User
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new User. 
  */
  async create(req, res) {
    let validation = await UserValidation.create(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let user = await User
      .create({
        nome: req.body.nome,
        email: req.body.email,
        is_adm: false
      })
      .then(user => User.findOne({
        where: { id: user.id },
        attributes: { exclude: ['senha'] }
      }));

    return res.status(201).json(user);
  }

  /**
   * Creates an administrative user
   * 
   * @param {Express.Request} req Request received from the client.
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the new User. 
  */
  async createAdmin(req, res) {
    let validation = await UserValidation.createAdmin(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let user = await User
      .create({
        nome: req.body.nome,
        email: req.body.email,
        senha: await bcrypt.hash(req.body.senha, 8),
        is_adm: true
      })
      .then(user => User.findOne({
        where: { id: user.id },
        attributes: { exclude: ['senha'] }
      }));

    return res.status(201).json(user);
  }

  /**
  * Return all Users
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns all Users. 
  */
  async readAll(req, res) {
    let users = await User.findAll({ attributes: { exclude: ['senha'] } });

    return res.status(200).json(users);
  }

  /**
  * Return a specific User
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns a specififc User. 
  */
  async readOne(req, res) {
    let user = await User.findOne({
      where: {
        id: req.params.id,
      },
      attributes: { exclude: ['senha'] }
    });

    return res.status(200).json(user);
  }

  /**
  * Update an User
  * 
  * @param {Express.Request} req Request received from the client.
  * @param {Express.Response} res Response to be returned to the client.
  * @returns {Express.Response} Returns the updated User. 
  */
  async updateName(req, res) {
    let validation = await UserValidation.updateName(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let oldUser = await User.findOne({
      where: {
        id: req.params.id
      },
    })

    if (oldUser) {
      let newUser = await oldUser
        .update({ nome: req.body.nome })
        .then(user => User.findOne({
          where: { id: user.id },
          attributes: { exclude: ['senha'] }
        }));

      return res.status(200).json(newUser);
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });  
    }
  }

  /**
   * Delete User
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns delete message. 
   */

  async delete(req, res) {
    let user = await User.findOne({
      where: {
        id: req.params.id
      }
    })

    if (user) {
      await user.destroy();
      return res.status(200).json({ message: messages.success.deleted });
    } else {
      return res.status(404).json({ message: messages.error.dataNotFound });
    }
  }

  /**
   * Login
   * 
   * @param {Express.Request} req Request received from the client
   * @param {Express.Response} res Response to be returned to the client.
   * @returns {Express.Response} Returns the logged in user. 
  */
  async login(req, res) {
    let validation = await UserValidation.login(req.body);
    if (!validation.valid) {
      delete validation.valid;
      return res.status(400).json(validation);
    }

    let { id, name, email, is_adm } = validation.user;
    let response = {
      user: {
        id,
        name,
        email
      }
    };

    if (is_adm) {
      response.token = jwt.sign({ id }, Auth.secret, { expiresIn: Auth.expiresIn });
    }

    return res.status(200).json(response);
  }
}

module.exports = new UserController();
