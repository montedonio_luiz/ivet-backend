const { Sequelize, Model,  } = require('sequelize');

class MarcaRacao extends Model {
  static init(sequelize) {
    super.init({
      nome: Sequelize.STRING,
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'marca_racao',
      }
    )
  }

  static associate(models) {
    this.hasOne(models.Racao)
  }
};

module.exports = MarcaRacao;