const { Sequelize, Model,  } = require('sequelize');

class Especie extends Model {
  static init(sequelize) {
    super.init({
      nome: Sequelize.STRING,
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'especie',
      }
    )
  }

  static associate(models) {
    this.hasOne(models.Racao);
    this.hasOne(models.Medicamento);
  }
};

module.exports = Especie;