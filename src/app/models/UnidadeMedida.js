const { Sequelize, Model,  } = require('sequelize');

class UnidadeMedida extends Model {
  static init(sequelize) {
    super.init({
      nome: Sequelize.STRING,
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'unidade_medida',
      }
    )
  }

  static associate(models) {
    this.hasOne(models.Medicamento);
  }
};

module.exports = UnidadeMedida;