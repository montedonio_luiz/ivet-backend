const { Sequelize, Model,  } = require('sequelize');

class Frequencia extends Model {
  static init(sequelize) {
    super.init({
      nome: Sequelize.STRING,
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'frequencia',
      }
    )
  }

  static associate(models) {
    this.belongsToMany(models.Medicamento, { through: 'medicamento_frequencia', foreignKey: 'frequencia_id', as: 'Medicamentos', timestamps: false, });
  }
};

module.exports = Frequencia;