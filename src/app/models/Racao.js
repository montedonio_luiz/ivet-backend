const { Sequelize, Model,  } = require('sequelize');

class Racao extends Model {
  static init(sequelize) {
    super.init({
      nome: Sequelize.STRING,
      energia_metabolizavel: Sequelize.FLOAT
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'racao',
      }
    )
  }

  static associate(models) {
    this.belongsTo(models.MarcaRacao, {foreignKey: 'marca_racao_id'});
    this.belongsTo(models.Especie, {foreignKey: 'especie_id'});
  }
};

module.exports = Racao;