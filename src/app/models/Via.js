const { Sequelize, Model, } = require('sequelize');

class Via extends Model {
  static init(sequelize) {
    super.init({
      nome: Sequelize.STRING,
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'via',
      }
    )
  }

  static associate(models) {
    this.belongsToMany(models.Medicamento, { through: 'medicamento_via', foreignKey: 'via_id', as: 'Medicamentos', timestamps: false, });
  }
};

module.exports = Via;