const { Sequelize, Model,  } = require('sequelize');

class User extends Model {
  static init(sequelize) {
    super.init({
      nome:  Sequelize.STRING,
      email: Sequelize.STRING,
      senha: Sequelize.STRING,
      is_adm: Sequelize.BOOLEAN,
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'user',
      }
    )
  }
};

module.exports = User;