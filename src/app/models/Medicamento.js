const { Sequelize, Model, } = require('sequelize');

class Medicamento extends Model {
  static init(sequelize) {
    super.init({
      nome: Sequelize.STRING,
      dose_min: Sequelize.FLOAT,
      dose_max: Sequelize.FLOAT,
      dose_unica: Sequelize.FLOAT,
      concentracao: Sequelize.FLOAT,
    },
      {
        sequelize,
        underscored: true,
        timestamps: false,
        tableName: 'medicamento',
      }
    )
  }

  static associate(models) {
    this.belongsTo(models.Especie, { foreignKey: 'especie_id' });
    this.belongsTo(models.UnidadeMedida, { foreignKey: 'unidade_medida_id' });

    this.belongsToMany(models.Frequencia, { through: 'medicamento_frequencia', foreignKey: 'medicamento_id', as: 'Frequencias' });
    this.belongsToMany(models.Via, { through: 'medicamento_via', foreignKey: 'medicamento_id', as: 'Vias' });
  }
};

module.exports = Medicamento;