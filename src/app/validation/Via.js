// Dependencies
let yup = require('yup');
let messages = require('../utils/messages');

// Models
const Via = require('../models/Via');

yup.addMethod(yup.string, 'existingNameVia', function (message) {
  return this.test('existingNameVia', message, async function (value) {
    return !(await Via.findOne({ where: { nome: value } }));
  });
});

yup.addMethod(yup.string, 'existingNameViaUpdate', function (viaId, message) {
  return this.test('existingNameViaUpdate', message, async function (value) {
    let vias = await Via.findAll({ where: { nome: value } });

    return !(vias.length > 0 && vias[0].id != viaId);
  });
});

class ViaValidation {
  async create(body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.via.nomeRequired)
        .existingNameVia(messages.validation.via.existingName)
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async update(viaId, body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.via.nomeRequired)
        .existingNameViaUpdate(viaId, messages.validation.via.existingName),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

}

module.exports = new ViaValidation();