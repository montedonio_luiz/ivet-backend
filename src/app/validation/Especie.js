// Dependencies
let yup = require('yup');
let messages = require('../utils/messages');

// Models
const Especie = require('../models/Especie');

yup.addMethod(yup.string, 'existingNameEspecie', function (message) {
  return this.test('existingNameEspecie', message, async function (value) {
    return !(await Especie.findOne({ where: { nome: value } }));
  });
});

yup.addMethod(yup.string, 'existingNameEspecieUpdate', function (especieId, message) {
  return this.test('existingNameEspecieUpdate', message, async function (value) {
    let especies = await Especie.findAll({ where: { nome: value } });

    return !(especies.length > 0 && especies[0].id != especieId);
  });
});

class EspecieValidation {
  async create(body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.especie.nomeRequired)
        .existingNameEspecie(messages.validation.especie.existingName)
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async update(especieId, body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.especie.nomeRequired)
        .existingNameEspecieUpdate(especieId, messages.validation.especie.existingName),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

}

module.exports = new EspecieValidation();