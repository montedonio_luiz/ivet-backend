// Dependencies
let yup = require('yup');
let messages = require('../utils/messages');

// Models
const MarcaRacao = require('../models/MarcaRacao');

yup.addMethod(yup.string, 'existingNameMarcaRacao', function (message) {
  return this.test('existingNameMarcaRacao', message, async function (value) {
    return !(await MarcaRacao.findOne({ where: { nome: value } }));
  });
});

yup.addMethod(yup.string, 'existingNameMarcaRacaoUpdate', function (marcaRacaoId, message) {
  return this.test('existingNameMarcaRacaoUpdate', message, async function (value) {
    let marcaRacaos = await MarcaRacao.findAll({ where: { nome: value } });

    return !(marcaRacaos.length > 0 && marcaRacaos[0].id != marcaRacaoId);
  });
});

class MarcaRacaoValidation {
  async create(body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.marca_racao.nomeRequired)
        .existingNameMarcaRacao(messages.validation.marca_racao.existingName)
    });
    
    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async update(marcaRacaoId, body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.marca_racao.nomeRequired)
        .existingNameMarcaRacaoUpdate(marcaRacaoId, messages.validation.marca_racao.existingName),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

}

module.exports = new MarcaRacaoValidation();