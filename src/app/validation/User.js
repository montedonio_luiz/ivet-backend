// Dependencies
let yup = require('yup');
let bcrypt = require('bcryptjs');
let messages = require('../utils/messages');

// Models
const User = require('../models/User');

yup.addMethod(yup.string, 'existingEmail', function (message) {
  return this.test('existingEmail', message, async function (value) {
    return !(await User.findOne({ where: { email: value } }));
  });
});

yup.addMethod(yup.string, 'isAdm', function (message) {
  return this.test('isAdm', message, async function (value) {
    let user = await User.findOne({ where: { email: value } });
    
    if (user) {
      return user.is_adm;
    } else {
      return false;
    }
  });
});

class UserValidation {
  async createAdmin(body) {
    const schema = yup.object().shape({
      nome: yup.string().required(messages.validation.user.nomeRequired),
      email: yup.string()
        .email(messages.validation.user.validEmail)
        .required(messages.validation.user.emailRequired)
        .existingEmail(messages.validation.user.existingEmail),
      senha: yup.string().required(messages.validation.user.senhaRequired),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async create(body) {
    const schema = yup.object().shape({
      nome: yup.string().required(messages.validation.user.nomeRequired),
      email: yup.string()
        .email(messages.validation.user.validEmail)
        .required(messages.validation.user.emailRequired)
        .existingEmail(messages.validation.user.existingEmail),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async updateName(body) {
    const schema = yup.object().shape({
      nome: yup.string().required(messages.validation.user.nomeRequired),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async login(body) {
    const schema = yup.object().shape({
      email: yup.string()
        .email(messages.validation.user.validEmail)
        .required(messages.validation.user.emailRequired)
        .isAdm(messages.validation.user.mustBeAdm),
      senha: yup.string().required(messages.validation.user.senhaRequired),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(async _ => {
        let user = await User.findOne({ where: { email: body.email } });

        if (!user) {
          return { errors: [messages.error.login], valid: false };
        }

        if (!(await bcrypt.compare(body.senha, user.senha))) {
          return { errors: [messages.error.login], valid: false };
        } else {
          return { valid: true, user };
        }
      })
      .catch(err => {
        console.log("ERROR=> ", err)
        return { errors: err.errors, valid: false }
      })
  }
}

module.exports = new UserValidation();