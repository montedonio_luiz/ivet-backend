// Dependencies
let yup = require('yup');
let messages = require('../utils/messages');

// Models
const Racao = require('../models/Racao');
const Especie = require('../models/Especie');
const MarcaRacao = require('../models/MarcaRacao');

yup.addMethod(yup.string, 'existingName_Racao', function (message) {
  return this.test('existingName_Racao', message, async function (value) {
    return !(await Racao.findOne({ where: { nome: value } }));
  });
});

yup.addMethod(yup.string, 'existingName_RacaoUpdate', function (racaoId, message) {
  return this.test('existingName_RacaoUpdate', message, async function (value) {
    let racaos = await Racao.findAll({ where: { nome: value } });

    return !(racaos.length > 0 && racaos[0].id != racaoId);
  });
});

yup.addMethod(yup.number, 'existingEspecie_Racao', function (message) {
  return this.test('existingEspecie_Racao', message, async function (value) {
    if (value) {
      return (await Especie.findOne({ where: { id: value } }))
    } else {
      return false
    }
  });
});

yup.addMethod(yup.number, 'existingMarcaRacao_Racao', function (message) {
  return this.test('existingMarcaRacao_Racao', message, async function (value) {
    if (value) {
      return (await MarcaRacao.findOne({ where: { id: value } }))
    } else {
      return false
    }
  });
});

class RacaoValidation {
  async create(body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.racao.nomeRequired)
        .existingName_Racao(messages.validation.racao.existingName),
      marca_racao_id: yup
        .number().typeError(messages.validation.racao.marcaRacao.number)
        .integer(messages.validation.racao.marcaRacao.integer)
        .required(messages.validation.racao.marcaRacao.required)
        .existingMarcaRacao_Racao(messages.validation.racao.marcaRacao.existing),
      especie_id: yup
        .number().typeError(messages.validation.racao.especie.number)
        .integer(messages.validation.racao.especie.integer)
        .required(messages.validation.racao.especie.required)
        .existingEspecie_Racao(messages.validation.racao.especie.existing),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async update(racaoId, body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.racao.nomeRequired)
        .existingName_RacaoUpdate(racaoId, messages.validation.racao.existingName),
      marca_racao_id: yup
        .number().typeError(messages.validation.racao.marcaRacao.number)
        .integer(messages.validation.racao.marcaRacao.integer)
        .required(messages.validation.racao.marcaRacao.required)
        .existingMarcaRacao_Racao(messages.validation.racao.marcaRacao.existing),
      especie_id: yup
        .number().typeError(messages.validation.racao.especie.number)
        .integer(messages.validation.racao.especie.integer)
        .required(messages.validation.racao.especie.required)
        .existingEspecie_Racao(messages.validation.racao.especie.existing),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

}

module.exports = new RacaoValidation();