// Dependencies
let yup = require('yup');
let messages = require('../utils/messages');

// Models
const Via = require('../models/Via');
const Especie = require('../models/Especie');
const Frequencia = require('../models/Frequencia');
const Medicamento = require('../models/Medicamento');
const UnidadeMedida = require('../models/UnidadeMedida');

// Custom Validations
yup.addMethod(yup.string, 'existingName_Medicamento', function (message) {
  return this.test('existingName_Medicamento', message, async function (value) {
    return !(await Medicamento.findOne({ where: { nome: value } }));
  });
});

yup.addMethod(yup.string, 'existingName_MedicamentoUpdate', function (medicamentoId, message) {
  return this.test('existingName_MedicamentoUpdate', message, async function (value) {
    let medicamentos = await Medicamento.findAll({ where: { nome: value } });

    return !(medicamentos.length > 0 && medicamentos[0].id != medicamentoId);
  });
});

yup.addMethod(yup.number, 'existingEspecie_Medicamento', function (message) {
  return this.test('existingEspecie_Medicamento', message, async function (value) {
    if (value) {
      return (await Especie.findOne({ where: { id: value } }))
    } else {
      return false
    }
  });
});

yup.addMethod(yup.number, 'existingVia_Medicamento', function (message) {
  return this.test('existingVia_Medicamento', message, async function (value) {
    if (value) {
      return (await Via.findOne({ where: { id: value } }))
    } else {
      return false
    }
  });
});

yup.addMethod(yup.number, 'existingFrequencia_Medicamento', function (message) {
  return this.test('existingFrequencia_Medicamento', message, async function (value) {
    if (value) {
      return (await Frequencia.findOne({ where: { id: value } }))
    } else {
      return false
    }
  });
});

yup.addMethod(yup.number, 'existingUnidadeMedida_Medicamento', function (message) {
  return this.test('existingUnidadeMedida_Medicamento', message, async function (value) {
    if (value) {
      return (await UnidadeMedida.findOne({ where: { id: value } }))
    } else {
      return false
    }
  });
});

class MedicamentoValidation {
  async create(body) {
    const schema = yup.object().shape({
      nome: yup
        .string()
        .required(messages.validation.medicamento.nomeRequired)
        .existingName_Medicamento(messages.validation.medicamento.existingName),
      dose_min: yup
        .number().typeError(messages.validation.medicamento.doseMin.number)
        .nullable(),
      dose_max: yup
        .number().typeError(messages.validation.medicamento.doseMax.number)
        .nullable(),
      dose_unica: yup
        .number().typeError(messages.validation.medicamento.doseMax.number)
        .nullable(),
      concentracao: yup
        .number().typeError(messages.validation.medicamento.concentracaoNumber)
        .nullable(),
      especie_id: yup
        .number().typeError(messages.validation.medicamento.especie.number)
        .integer(messages.validation.medicamento.especie.integer)
        .required(messages.validation.medicamento.especie.required)
        .existingEspecie_Medicamento(messages.validation.medicamento.especie.existing),
      unidade_medida_id: yup
        .number().typeError(messages.validation.medicamento.unidade_medida.number)
        .integer(messages.validation.medicamento.unidade_medida.integer)
        .required(messages.validation.medicamento.unidade_medida.required)
        .existingUnidadeMedida_Medicamento(messages.validation.medicamento.unidade_medida.existing),
      Vias: yup
        .array().typeError(messages.validation.medicamento.vias.array)
        .of(yup
          .number().typeError(messages.validation.medicamento.vias.number)
          .integer(messages.validation.medicamento.vias.integer)
          .existingVia_Medicamento(messages.validation.medicamento.vias.existing)
        )
        .required(messages.validation.medicamento.vias.required)
        .min(1, messages.validation.medicamento.vias.minimum),
      Frequencias: yup
        .array().typeError(messages.validation.medicamento.frequencias.array)
        .of(yup
          .number().typeError(messages.validation.medicamento.frequencias.number)
          .integer(messages.validation.medicamento.frequencias.integer)
          .existingFrequencia_Medicamento(messages.validation.medicamento.frequencias.existing)
        )
        .min(1, messages.validation.medicamento.frequencias.minimum)
    });
    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async update(medicamentoId, body) {
    const schema = yup.object().shape({
      nome: yup
        .string()
        .required(messages.validation.medicamento.nomeRequired)
        .existingName_MedicamentoUpdate(medicamentoId, messages.validation.medicamento.existingName),
      dose_min: yup
        .number().typeError(messages.validation.medicamento.doseMin.number)
        .nullable(),
      dose_max: yup
        .number().typeError(messages.validation.medicamento.doseMax.number)
        .nullable(),
      dose_unica: yup
        .number().typeError(messages.validation.medicamento.doseMax.number)
        .nullable(),
      concentracao: yup
        .number().typeError(messages.validation.medicamento.concentracaoNumber)
        .nullable(),
      especie_id: yup
        .number().typeError(messages.validation.medicamento.especie.number)
        .integer(messages.validation.medicamento.especie.integer)
        .required(messages.validation.medicamento.especie.required)
        .existingEspecie_Medicamento(messages.validation.medicamento.especie.existing),
      Vias: yup
        .array().typeError(messages.validation.medicamento.vias.array)
        .of(yup
          .number().typeError(messages.validation.medicamento.vias.number)
          .integer(messages.validation.medicamento.vias.integer)
          .existingVia_Medicamento(messages.validation.medicamento.vias.existing)
        )
        .required(messages.validation.medicamento.vias.required)
        .min(1, messages.validation.medicamento.vias.minimum),
      Frequencias: yup
        .array().typeError(messages.validation.medicamento.frequencias.array)
        .of(yup
          .number().typeError(messages.validation.medicamento.frequencias.number)
          .integer(messages.validation.medicamento.frequencias.integer)
          .existingFrequencia_Medicamento(messages.validation.medicamento.frequencias.existing)
        )
        .min(1, messages.validation.medicamento.frequencias.minimum)
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

}

module.exports = new MedicamentoValidation();