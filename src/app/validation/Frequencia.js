// Dependencies
let yup = require('yup');
let messages = require('../utils/messages');

// Models
const Frequencia = require('../models/Frequencia');

yup.addMethod(yup.string, 'existingNameFrequencia', function (message) {
  return this.test('existingNameFrequencia', message, async function (value) {
    return !(await Frequencia.findOne({ where: { nome: value } }));
  });
});

yup.addMethod(yup.string, 'existingNameFrequenciaUpdate', function (frequenciaId, message) {
  return this.test('existingNameFrequenciaUpdate', message, async function (value) {
    let frequencias = await Frequencia.findAll({ where: { nome: value } });

    return !(frequencias.length > 0 && frequencias[0].id != frequenciaId);
  });
});

class FrequenciaValidation {
  async create(body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.frequencia.nomeRequired)
        .existingNameFrequencia(messages.validation.frequencia.existingName)
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

  async update(frequenciaId, body) {
    const schema = yup.object().shape({
      nome: yup.string()
        .required(messages.validation.frequencia.nomeRequired)
        .existingNameFrequenciaUpdate(frequenciaId, messages.validation.frequencia.existingName),
    });

    return await schema.validate(body, { abortEarly: false })
      .then(_ => ({ valid: true }))
      .catch(err => ({ errors: err.errors, valid: false }))
  }

}

module.exports = new FrequenciaValidation();