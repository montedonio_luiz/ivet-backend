module.exports = {
  success: {
    deleted: 'Registro removido com sucesso.'
  },
  error: {
    login: 'E-mail inexistente ou senha incorreta.',
    dataNotFound: 'Este registro é inexistente.',
    token: {
      missing: 'Token de autorização não foi recebido.',
      invalid: 'Token de autorização inválido.'
    }
  },
  validation: {
    user: {
      nomeRequired: 'O nome do usuário é obrigatório.',
      validEmail: 'O e-mail deve ser válido.',
      emailRequired: 'O e-mail é obrigatório.',
      senhaRequired: 'A senha é obrigatória.',
      existingEmail: 'Este e-mail já existe.',
      mustBeAdm: 'Apenas usuários administrativos podem realizar log-in.',
    },

    marca_racao: {
      nomeRequired: 'O nome da marca é obrigatório.',
      existingName: 'Esta marca já existe.',
    },

    via: {
      nomeRequired: 'O nome da via é obrigatório.',
      existingName: 'Esta via já existe.',
    },

    especie: {
      nomeRequired: 'O nome da espécie é obrigatório.',
      existingName: 'Esta espécie já existe.',
    },

    racao: {
      nomeRequired: 'O nome da ração é obrigatório.',
      existingName: 'Esta ração já existe.',
      marcaRacao: {
        number: 'Erro de sistema: O id da marca da ração referente à ração deve ser um número válido.',
        integer: 'Erro de sistema: O id da marca da ração referente à ração deve ser um número inteiro.',
        required: 'A marca da ração é obrigatória',
        existing: 'Esta marca de ração não existe.'
      },
      especie: {
        number: 'Erro de sistema: O id da espécie referente à ração deve ser um número válido.',
        integer: 'Erro de sistema: O id da espécie referente à ração deve ser um número inteiro.',
        required: 'A espécie é obrigatória',
        existing: 'Esta espécie não existe.'
      }
    },

    frequencia: {
      nomeRequired: 'O nome da frequência é obrigatório.',
      existingName: 'Esta frequência já existe.',
    },

    medicamento: {
      nomeRequired: 'O nome do medicamento é obrigatório.',
      existingName: 'Este medicamento já existe.',
      doseMin: {
        number: 'A dose mínima do medicamento deve ser um número válido.',
      },
      doseMax: {
        number: 'A dose máxima do medicamento deve ser um número válido.',
      },
      doseUnica: {
        number: 'A dose única do medicamento deve ser um número válido.',
      },
      concentracaoNumber: 'A concentração do medicamento deve ser um número válido.',
      especie: {
        number: 'Erro de sistema: O id da espécie referente ao medicamento deve ser um número válido.',
        integer: 'Erro de sistema: O id da espécie referente ao medicamento deve ser um número inteiro.',
        required: 'A espécie para qual o medicamento é direcionado é obrigatória.',
        existing: 'Esta espécie não existe.'
      },
      unidade_medida: {
        number: 'Erro de sistema: O id da unidade de medida referente ao medicamento deve ser um número válido.',
        integer: 'Erro de sistema: O id da unidade de medida referente ao medicamento deve ser um número inteiro.',
        required: 'A unidade de medida para qual o medicamento é direcionado é obrigatória.',
        existing: 'Esta unidade de medida não existe.'
      },
      vias: {
        array: 'Erro de sistema: As vias devem ser uma lista.',
        required: 'As vias são obrigatórias.',
        number: 'Erro de sistema: ${path} deve ser um número válido.',
        integer: 'Erro de sistema: ${path} deve ser um número inteiro.',
        existing: 'Erro de sistema: ${path} não existe.',
        minimum: 'Pelo menos uma via deve ser registrada com o medicamento.'
      },
      frequencias: {
        array: 'Erro de sistema: As frequências devem ser uma lista.',
        number: 'Erro de sistema: ${path} deve ser um número válido.',
        integer: 'Erro de sistema: ${path} deve ser um número inteiro.',
        existing: 'Erro de sistema: ${path} não existe.',
        minimum: 'Pelo menos uma frequência deve ser registrada com o medicamento.'
      }
    },

  }
}