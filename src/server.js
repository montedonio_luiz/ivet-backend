// Dependencies
const express = require('express');
const cors = require('cors');

const routes = require('./routes');

require('./database');


class App {
  constructor() {
    this.isDev = process.env.NODE_ENV !== 'production';
    
    this.express = express();
    this.express.use(cors());

    this.express.use(express.json())
    
    this.express.use(routes);

  }
}
module.exports = new App().express;
