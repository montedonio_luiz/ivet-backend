// Dependencies
const { Router } = require('express');
const authMiddleware = require('./app/middlewares/auth');

const routes = new Router();

const viaBaseUrl = '/via';
const userBaseUrl = '/user';
const racaoBaseUrl = '/racao';
const especieBaseUrl = '/especie';
const frequenciaBaseUrl = '/frequencia';
const medicamentoBaseUrl = '/medicamento';
const unidadeMedidaUrl = '/unidadeMedida';
const marcaRacaoBaseUrl = `${racaoBaseUrl}/marca`;

// Controllers
const ViaController = require('./app/controllers/ViaController');
const UserController = require('./app/controllers/UserController');
const RacaoController = require('./app/controllers/RacaoController');
const EspecieController = require('./app/controllers/EspecieController');
const FrequenciaController = require('./app/controllers/FrequenciaController');
const MarcaRacaoController = require('./app/controllers/MarcaRacaoController');
const MedicamentoController = require('./app/controllers/MedicamentoController');
const UnidadeMedidaController = require('./app/controllers/UnidadeMedidaController');

// *********************************************************************************************************
// ************************************************ ROUTES *************************************************
// *********************************************************************************************************

// UNAUTHORIZED ROUTES

// Hello World
routes.get('/', (req, res) => res.send('Hello World'));

// Login
routes.post('/login', UserController.login);

// Especie
routes.get(especieBaseUrl, EspecieController.readAll);
routes.get(`${especieBaseUrl}/:id`, EspecieController.readOne);

// Frequencia
routes.get(frequenciaBaseUrl, FrequenciaController.readAll);
routes.get(`${frequenciaBaseUrl}/:id`, FrequenciaController.readOne);

// Via
routes.get(viaBaseUrl, ViaController.readAll);
routes.get(`${viaBaseUrl}/:id`, ViaController.readOne);

// User
routes.post(userBaseUrl, UserController.create);

// MarcaRacao
routes.get(marcaRacaoBaseUrl, MarcaRacaoController.readAll);
routes.get(`${marcaRacaoBaseUrl}/:id`, MarcaRacaoController.readOne);

// Racao
routes.get(racaoBaseUrl, RacaoController.readAll);
routes.get(`${racaoBaseUrl}/:id`, RacaoController.readOne);

// Medicamento
routes.get(medicamentoBaseUrl, MedicamentoController.readAll);
routes.get(`${medicamentoBaseUrl}/:id`, MedicamentoController.readOne);

// UnidadeMedida
routes.get(unidadeMedidaUrl, UnidadeMedidaController.readAll);

// Verify jwt
routes.use(authMiddleware.authenticate);

// AUTHORIZED ROUTES

// Especie
routes.post(especieBaseUrl, EspecieController.create);
routes.put(`${especieBaseUrl}/:id`, EspecieController.update);
routes.delete(`${especieBaseUrl}/:id`, EspecieController.delete);

// Frequencia
routes.post(frequenciaBaseUrl, FrequenciaController.create);
routes.put(`${frequenciaBaseUrl}/:id`, FrequenciaController.update);
routes.delete(`${frequenciaBaseUrl}/:id`, FrequenciaController.delete);

// Via
routes.post(viaBaseUrl, ViaController.create);
routes.put(`${viaBaseUrl}/:id`, ViaController.update);
routes.delete(`${viaBaseUrl}/:id`, ViaController.delete);

// User
routes.post(`${userBaseUrl}/admin`, UserController.createAdmin);
routes.get(`${userBaseUrl}/:id`, UserController.readOne);
routes.get(userBaseUrl, UserController.readAll);
routes.put(`${userBaseUrl}/:id/name`, UserController.updateName);
routes.delete(`${userBaseUrl}/:id`, UserController.delete);

// MarcaRacao
routes.post(marcaRacaoBaseUrl, MarcaRacaoController.create);
routes.put(`${marcaRacaoBaseUrl}/:id`, MarcaRacaoController.update);
routes.delete(`${marcaRacaoBaseUrl}/:id`, MarcaRacaoController.delete);

// Racao
routes.post(racaoBaseUrl, RacaoController.create);
routes.put(`${racaoBaseUrl}/:id`, RacaoController.update);
routes.delete(`${racaoBaseUrl}/:id`, RacaoController.delete);

// Medicamento
routes.post(medicamentoBaseUrl, MedicamentoController.create);
routes.put(`${medicamentoBaseUrl}/:id`, MedicamentoController.update);
routes.delete(`${medicamentoBaseUrl}/:id`, MedicamentoController.delete);


module.exports = routes;
