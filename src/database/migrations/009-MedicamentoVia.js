'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('medicamento_via', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      medicamento_id : {
        type: Sequelize.INTEGER,
        references: {
          model: 'medicamento',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
      via_id : {
        type: Sequelize.INTEGER,
        references: {
          model: 'via',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
    });
  },

  down: (queryInterface, _) => {
    return queryInterface.dropTable('medicamento_via');
  }
};
