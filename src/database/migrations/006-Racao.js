'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('racao', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      energia_metabolizavel: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      marca_racao_id : {
        type: Sequelize.INTEGER,
        references: {
          model: 'marca_racao',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
      especie_id : {
        type: Sequelize.INTEGER,
        references: {
          model: 'especie',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
    });
  },

  down: (queryInterface, _) => {
    return queryInterface.dropTable('racao');
  }
};