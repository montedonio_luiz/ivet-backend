'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('medicamento', 'dose_unica', {
          type: Sequelize.DataTypes.FLOAT,
          allowNull: true,
        }, { transaction: t }),
        queryInterface.addColumn('medicamento', 'unidade_medida_id', {
          type: Sequelize.INTEGER,
          references: {
            model: 'unidade_medida',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
          allowNull: false
        }, { transaction: t })
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('medicamento', 'dose_unica', { transaction: t }),
        queryInterface.removeColumn('medicamento', 'unidade_medida_id', { transaction: t })
      ]);
    });
  }
};