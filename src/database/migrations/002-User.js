'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      senha: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      is_adm: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
    });
  },

  down: (queryInterface, _) => {
    return queryInterface.dropTable('user');
  }
};
