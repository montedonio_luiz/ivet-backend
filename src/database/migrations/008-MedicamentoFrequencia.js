'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('medicamento_frequencia', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      medicamento_id : {
        type: Sequelize.INTEGER,
        references: {
          model: 'medicamento',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
      frequencia_id : {
        type: Sequelize.INTEGER,
        references: {
          model: 'frequencia',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
    });
  },

  down: (queryInterface, _) => {
    return queryInterface.dropTable('medicamento_frequencia');
  }
};
