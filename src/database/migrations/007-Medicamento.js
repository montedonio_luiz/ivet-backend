'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('medicamento', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      dose_min: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      dose_max: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      concentracao: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      especie_id : {
        type: Sequelize.INTEGER,
        references: {
          model: 'especie',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
    });
  },

  down: (queryInterface, _) => {
    return queryInterface.dropTable('medicamento');
  }
};