// Dependencies
require('dotenv/config');
const { Sequelize } = require('sequelize');

// Models
const Via = require('../app/models/Via');
const User = require('../app/models/User');
const Racao = require('../app/models/Racao');
const Especie = require('../app/models/Especie');
const Frequencia = require('../app/models/Frequencia');
const MarcaRacao = require('../app/models/MarcaRacao');
const Medicamento = require('../app/models/Medicamento');
const UnidadeMedida = require('../app/models/UnidadeMedida');

const models = [
  Via,
  User,
  Racao,
  Especie,
  Frequencia,
  MarcaRacao,
  Medicamento,
  UnidadeMedida
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(process.env.NODE_ENV === 'production' ? process.env.DATABASE_URL : process.env.DB_URL_DEV);

    models.map(model => model.init(this.connection));
    models.map(model => model.associate && model.associate(this.connection.models));
  }

  sequelize() {
    return this.connection
  }
}

module.exports = new Database();