'use strict';

const db = require('../index');
const tableName = 'medicamento_via';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = [
      { medicamento_id: 1, via_id: 4, },
      { medicamento_id: 2, via_id: 3, },
      { medicamento_id: 3, via_id: 4, },
      { medicamento_id: 3, via_id: 4, },
      { medicamento_id: 4, via_id: 4, },
      { medicamento_id: 4, via_id: 4, },
      { medicamento_id: 5, via_id: 4, },
      { medicamento_id: 6, via_id: 4, },
      { medicamento_id: 7, via_id: 4, },
      { medicamento_id: 7, via_id: 1, },
      { medicamento_id: 7, via_id: 3, },
      { medicamento_id: 8, via_id: 3, },
      { medicamento_id: 8, via_id: 1, },
      { medicamento_id: 8, via_id: 2, },
      { medicamento_id: 9, via_id: 4, },
      { medicamento_id: 10, via_id: 1, },
      { medicamento_id: 10, via_id: 2, },
      { medicamento_id: 10, via_id: 4, },
      { medicamento_id: 11, via_id: 1, },
      { medicamento_id: 11, via_id: 2, },
      { medicamento_id: 11, via_id: 4, },
      { medicamento_id: 12, via_id: 4, },
      { medicamento_id: 13, via_id: 2, },
      { medicamento_id: 13, via_id: 1, },
      { medicamento_id: 13, via_id: 3, },
      { medicamento_id: 14, via_id: 2, },
      { medicamento_id: 14, via_id: 1, },
      { medicamento_id: 14, via_id: 3, },
      { medicamento_id: 15, via_id: 4, },
      { medicamento_id: 15, via_id: 3, },
      { medicamento_id: 16, via_id: 4, },
      { medicamento_id: 17, via_id: 1, },
      { medicamento_id: 18, via_id: 1, },
      { medicamento_id: 19, via_id: 1, },
      { medicamento_id: 20, via_id: 1, },
      { medicamento_id: 20, via_id: 4, },
      { medicamento_id: 20, via_id: 2, },
      { medicamento_id: 20, via_id: 3, },
      { medicamento_id: 21, via_id: 1, },
      { medicamento_id: 21, via_id: 4, },
      { medicamento_id: 22, via_id: 4, },
      { medicamento_id: 22, via_id: 3, },
      { medicamento_id: 23, via_id: 2, },
      { medicamento_id: 23, via_id: 4, },
      { medicamento_id: 24, via_id: 4, },
      { medicamento_id: 25, via_id: 3, },
      { medicamento_id: 25, via_id: 1, },
      { medicamento_id: 25, via_id: 2, },
      { medicamento_id: 26, via_id: 3, },
      { medicamento_id: 26, via_id: 1, },
      { medicamento_id: 26, via_id: 2, },
      { medicamento_id: 27, via_id: 3, },
      { medicamento_id: 27, via_id: 2, },
      { medicamento_id: 28, via_id: 2, },
      { medicamento_id: 29, via_id: 4, },
      { medicamento_id: 30, via_id: 3, },
      { medicamento_id: 30, via_id: 2, },
      { medicamento_id: 30, via_id: 1, },
      { medicamento_id: 30, via_id: 4, },
      { medicamento_id: 31, via_id: 3, },
      { medicamento_id: 31, via_id: 1, },
      { medicamento_id: 31, via_id: 4, },
      { medicamento_id: 32, via_id: 3, },
      { medicamento_id: 32, via_id: 2, },
      { medicamento_id: 32, via_id: 1, },
      { medicamento_id: 32, via_id: 4, },
      { medicamento_id: 33, via_id: 2, },
      { medicamento_id: 34, via_id: 3, },
      { medicamento_id: 34, via_id: 2, },
      { medicamento_id: 34, via_id: 1, },
      { medicamento_id: 34, via_id: 4, },
      { medicamento_id: 35, via_id: 3, },
      { medicamento_id: 35, via_id: 2, },
      { medicamento_id: 35, via_id: 1, },
      { medicamento_id: 36, via_id: 3, },
      { medicamento_id: 37, via_id: 4, },
      { medicamento_id: 37, via_id: 3, },
      { medicamento_id: 37, via_id: 1, },
      { medicamento_id: 37, via_id: 6, },
      { medicamento_id: 38, via_id: 4, },
      { medicamento_id: 39, via_id: 2, },
      { medicamento_id: 39, via_id: 4, },
      { medicamento_id: 40, via_id: 4, },
      { medicamento_id: 41, via_id: 4, },
      { medicamento_id: 42, via_id: 2, },
      { medicamento_id: 43, via_id: 3, },
      { medicamento_id: 43, via_id: 4, },
      { medicamento_id: 44, via_id: 3, },
      { medicamento_id: 44, via_id: 2, },
      { medicamento_id: 44, via_id: 1, },
      { medicamento_id: 44, via_id: 4, },
      { medicamento_id: 45, via_id: 3, },
      { medicamento_id: 45, via_id: 2, },
      { medicamento_id: 45, via_id: 1, },
      { medicamento_id: 45, via_id: 4, },
      { medicamento_id: 46, via_id: 4, },
      { medicamento_id: 47, via_id: 2, },
      { medicamento_id: 48, via_id: 2, },
      { medicamento_id: 49, via_id: 4, },
      { medicamento_id: 50, via_id: 3, },
      { medicamento_id: 50, via_id: 2, },
      { medicamento_id: 50, via_id: 1, },
      { medicamento_id: 50, via_id: 4, },
      { medicamento_id: 51, via_id: 3, },
      { medicamento_id: 51, via_id: 2, },
      { medicamento_id: 52, via_id: 3, },
      { medicamento_id: 52, via_id: 4, },
      { medicamento_id: 53, via_id: 1, },
      { medicamento_id: 53, via_id: 2, },
      { medicamento_id: 54, via_id: 4, },
      { medicamento_id: 55, via_id: 4, },
      { medicamento_id: 56, via_id: 4, },
      { medicamento_id: 57, via_id: 4, },
      { medicamento_id: 58, via_id: 3, },
      { medicamento_id: 59, via_id: 4, },
      { medicamento_id: 60, via_id: 4, },
      { medicamento_id: 61, via_id: 4, },
      { medicamento_id: 61, via_id: 2, },
      { medicamento_id: 62, via_id: 4, },
      { medicamento_id: 62, via_id: 2, },
      { medicamento_id: 63, via_id: 4, },
      { medicamento_id: 64, via_id: 3, },
      { medicamento_id: 64, via_id: 4, },
      { medicamento_id: 65, via_id: 4, },
      { medicamento_id: 66, via_id: 1, },
      { medicamento_id: 66, via_id: 2, },
      { medicamento_id: 67, via_id: 3, },
      { medicamento_id: 68, via_id: 3, },
      { medicamento_id: 69, via_id: 3, },
      { medicamento_id: 69, via_id: 4, },
      { medicamento_id: 70, via_id: 3, },
      { medicamento_id: 70, via_id: 4, },
      { medicamento_id: 71, via_id: 4, },
      { medicamento_id: 72, via_id: 1, },
      { medicamento_id: 72, via_id: 2, },
      { medicamento_id: 72, via_id: 4, },
      { medicamento_id: 73, via_id: 1, },
      { medicamento_id: 73, via_id: 2, },
      { medicamento_id: 73, via_id: 4, },
      { medicamento_id: 74, via_id: 1, },
      { medicamento_id: 74, via_id: 2, },
      { medicamento_id: 74, via_id: 4, },
      { medicamento_id: 75, via_id: 4, },
      { medicamento_id: 76, via_id: 4, },
      { medicamento_id: 77, via_id: 4, },
      { medicamento_id: 78, via_id: 3, },
      { medicamento_id: 78, via_id: 1, },
      { medicamento_id: 78, via_id: 4, },
      { medicamento_id: 79, via_id: 4, },
      { medicamento_id: 80, via_id: 4, },
      { medicamento_id: 81, via_id: 1, },
      { medicamento_id: 81, via_id: 2, },
      { medicamento_id: 82, via_id: 4, },
      { medicamento_id: 83, via_id: 4, },
      { medicamento_id: 84, via_id: 4, },
      { medicamento_id: 85, via_id: 4, },
      // { medicamento_id: 86, via_id: 4, },
      { medicamento_id: 87, via_id: 4, },
      { medicamento_id: 88, via_id: 4, },
      { medicamento_id: 89, via_id: 1, },
      { medicamento_id: 89, via_id: 2, },
      { medicamento_id: 90, via_id: 1, },
      { medicamento_id: 90, via_id: 2, },
    ]

    return queryInterface.bulkInsert(tableName, data, {})
      .then(_ => db.sequelize().query(`ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH ${data.length + 1}`));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};