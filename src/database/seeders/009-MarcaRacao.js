'use strict';

const db = require('../index');
const tableName = 'marca_racao';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = [
      { id: 1, nome: 'Royal Canin' },
      { id: 2, nome: 'Hills' },
      { id: 3, nome: 'Premieer Pet' },
      { id: 4, nome: 'Natural' },
      { id: 5, nome: 'Farmina - N & D Prime' },
      { id: 6, nome: 'Farmina - N & D Ocean' },
      { id: 7, nome: 'Farmina - N & D Pumkin' },
      { id: 8, nome: 'Farmina - N & D Ancestral Grain' },
      { id: 9, nome: 'Farmina - N & D Quinoa' },
      { id: 10, nome: 'Farmina - Cibau' },
      { id: 11, nome: 'Farmina - Ecopet Natural' },
      { id: 12, nome: 'Farmina - Vet Life' },
      { id: 13, nome: 'Farmina - Matisse' },
      { id: 14, nome: 'Pedigree' },
      { id: 15, nome: 'Optimum' },
      { id: 16, nome: 'Champ' },
      { id: 17, nome: 'Pet Delícia' },
      { id: 18, nome: 'Frost' },
      { id: 19, nome: 'Atacama' },
      { id: 20, nome: 'Astro' },
      { id: 21, nome: 'Bravo' },
      { id: 22, nome: 'Sapeca' },
      { id: 23, nome: 'Trotter' },
      { id: 24, nome: 'Nhock' },
      { id: 25, nome: 'Mac Dog' },
      { id: 26, nome: 'Firulais' },
      { id: 27, nome: 'Tango' },
      { id: 28, nome: 'Naturalis' },
      { id: 29, nome: 'Granplus' },
      { id: 30, nome: 'HercoSul - Biofresh' },
      { id: 31, nome: 'HercoSul - Three Dogs Super Premium' },
      { id: 32, nome: 'HercoSul - Three Dogs Premium Especial' },
      { id: 33, nome: 'Fino Trato'},
      { id: 34, nome: 'Purina - Pro Plan'},
      { id: 35, nome: 'Purina - Pro Plan Veterinary Diets'},
      { id: 36, nome: 'Purina - Cat Show'},
      { id: 37, nome: 'Whiskas'},
      { id: 38, nome: 'Kitekat'},
      { id: 39, nome: 'Dreamies'},
      { id: 40, nome: 'Le Roy'},
      { id: 41, nome: 'Sapeca'},
      { id: 42, nome: 'HercoSul - Three Cats Super Premium'},
      { id: 43, nome: 'Purina - Fancy Feast'},
    ]

    return queryInterface.bulkInsert(tableName, data, {})
      .then(_ => db.sequelize().query(`ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH ${data.length + 1}`));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
