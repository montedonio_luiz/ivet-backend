'use strict';

const db = require('../index');
const tableName = 'especie';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = [
      {
        id: 1,
        nome: 'Cão',
      },
      {
        id: 2,
        nome: 'Gato',
      },
    ]

    return queryInterface.bulkInsert(tableName, data, {})
      .then(_ => db.sequelize().query(`ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH ${data.length + 1}`));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
