'use strict';

const db = require('../index');
const tableName = 'frequencia';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = [
      { id: 1, nome: '1 hora' },
      { id: 2, nome: '4 hrs' },
      { id: 3, nome: '6 hrs' },
      { id: 4, nome: '8 hrs' },
      { id: 5, nome: '12 hrs' },
      { id: 6, nome: '24 hrs' },
      { id: 7, nome: '48 hrs' },
      { id: 8, nome: 'BID' },
      { id: 9, nome: 'TID' },
      { id: 10, nome: 'QID' },
      { id: 11, nome: 'SID' },
      { id: 12, nome: '14 dias' },
      { id: 13, nome: '7 dias' },
    ]

    return queryInterface.bulkInsert(tableName, data, {})
      .then(_ => db.sequelize().query(`ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH ${data.length + 1}`));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
