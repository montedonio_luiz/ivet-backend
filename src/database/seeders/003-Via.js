'use strict';

const db = require('../index');
const tableName = 'via';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = [
      { id: 1, nome: 'IM' },
      { id: 2, nome: 'SC' },
      { id: 3, nome: 'IV' },
      { id: 4, nome: 'VO' },
      { id: 5, nome: 'EV' },
      { id: 6, nome: 'RETAL' },
    ]

    return queryInterface.bulkInsert(tableName, data, {})
      .then(_ => db.sequelize().query(`ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH ${data.length + 1}`));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
