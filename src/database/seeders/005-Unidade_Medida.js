'use strict';

const db = require('../index');
const tableName = 'unidade_medida';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = [
      { id: 1, nome: 'MG' },
      { id: 2, nome: 'MCG' },
      { id: 3, nome: 'UI' },

    ]
    
    return queryInterface.bulkInsert(tableName, data, {})
      .then(_ => db.sequelize().query(`ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH ${data.length + 1}`));
  },

  
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
