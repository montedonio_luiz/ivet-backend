'use strict';

const db = require('../index');
const tableName = 'user';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = [
      {
        id: 1,
        nome: 'Usuário Administrativo Master',
        email: 'admin@ivet.com',
        senha: process.env.FIRST_USER_PASSWORD,
        is_adm: true,
      },
    ]

    return queryInterface.bulkInsert(tableName, data, {})
      .then(_ => db.sequelize().query(`ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH ${data.length + 1}`));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
