# Tutorial
This is a tutorial on how to run the project. Keep in mind that all steps **must** be followed in the order they're listed in order for the project to work. If you have any questions or if you find a bug in the system, feel free to open a issue.

# Node.js
Before running the project, you must have npm and node.js installed. Install @ https://nodejs.org/en/

You can test if the node dependencies were met by running the following commands and receiving their respective outputs:
```bash
npm -v
```
output (or similar):
```bash
npm -v
6.13.6
```
***
```bash
node -v
```
output (or similar):
```bash
node -v
v8.11.4
```
<br />

# PostgreSQL

You must also setup a local PostgreSQL database instance. Download PostgreSQL @ https://www.postgresql.org/download/

After downloading, you must create a local instance. You can create it using the PostgreSQL cli, or through the PgAdmin GUI. Download PgAdmin @ https://www.pgadmin.org/download/

Once you create your local database, you must reference it in the project. The project expects a valid PostgreSQL database URL. After creating the database, you might not have the URL, but you will have data like the database name, host, port, user to access it and its password, which is exactly what you need to build the URL.

Create a `.env` file in the project root and copy the contents from the `.env.example` file placed in the project root. It should look like this:
```
DATABASE_URL=postgres://username:password@host:port/database
DB_URL_DEV=postgres://username:password@host:port/database
JWT_KEY=THIS_IS_NOT_A_GOOD_KEY
FIRST_USER_PASSWORD=THIS_IS_NOT_A_GOOD_PASSWORD
```

These are additional custom environment variables that node can access through the `process.env` command in your code. You can see its use in `src/database/index.js`:
```javascript
this.connection = new Sequelize(process.env.NODE_ENV === 'production' ? process.env.DATABASE_URL : process.env.DB_URL_DEV);
```

As you can see, this is where the project calls the database environment variable and connects to your database. Since you will (should) only run this project locally, we will disregard the DATABASE_URL (used only in a production environment), and change the DB_URL_DEV variable. Node knows to look for this variable when running the project locally.

Don't worry about the last two variables just yet. We'll get to them in a bit.

After changing the DB_URL_DEV, in theory, when the project runs, it should automatically connect to the database.
<br />

# Running the project

To run the project, you must download its dependency packages from npm. Change directories into the project root and run the following command:
```bash
npm install
```

After that, to effectively run the project, run:
```bash
npm start
```
Which should get you the following output:
```npm
[nodemon] 2.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node src/index.js`
App listening on http://localhost:8000/
```

If you made it this far, the project is up and running and you can test the Hello World route: http://localhost:8000/

However, the project is not fully set up yet.
<br />

# JWT

The project uses JSON Web Token (JWT) for user authentication. You can read about it @ https://jwt.io/

In a nutshell, when you first login, the API will create and give you a token in the login response. You should use this token in the Authentication request header in all future requests that require authentication.

The routes that require authentication can be found in `src/routes.js`, after `line 64`:
```javascript
// Verify jwt
routes.use(authMiddleware.authenticate);
```

All routes in the file prior to this command do not need authentication and can be requested without sending the token.

In order for the project to generate and decrypt tokens, you must provide it with a secret key. You can find a web-site online that will generate and provide one for you, but honestly you can just type a sequence of random characters and it would work fine as well, for instance: `asdiydfg273434UTFnb567JFadas`.

Place your key in the JWT_KEY environment variable in the `.env` file.
<br />

# Migrations and Seeders

In order to structure the database with the project's entities, we must migrate them to the DB. The project uses Sequelize as its ORM manager.

You can run the migrations using either `npx` or `yarn`. We will use npx, as it should be downloaded along with npm.

Run the following:
```bash
npx sequelize-cli db:migrate
```

Which will output:
```bash
Sequelize CLI [Node: 8.11.4, CLI: 5.5.1, ORM: 5.21.5]

Loaded configuration file "src/config/database.js".
== 001-Especie: migrating =======
== 001-Especie: migrated (0.273s)

== 002-User: migrating =======
== 002-User: migrated (0.224s)

== 003-Frequencia: migrating =======
== 003-Frequencia: migrated (0.233s)

== 004-Via: migrating =======
== 004-Via: migrated (0.232s)

== 005-MarcaRacao: migrating =======
== 005-MarcaRacao: migrated (0.250s)

== 006-Racao: migrating =======
== 006-Racao: migrated (0.244s)

== 007-Medicamento: migrating =======
== 007-Medicamento: migrated (0.227s)

== 008-MedicamentoFrequencia: migrating =======
== 008-MedicamentoFrequencia: migrated (0.229s)

== 009-MedicamentoVia: migrating =======
== 009-MedicamentoVia: migrated (0.247s)
```

Now that we've successfully migrated our entities, let's populate it with a initial user so that we can login, receive a JWT, and start using the API.

Seeder files are pre-defined data objects that we want our database to start with. Access `src/database/seeders/001-User.js`
```javascript
'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user', [
      {
        nome: 'Usuário Administrativo Master',
        email: 'admin@ivet.com',
        senha: process.env.FIRST_USER_PASSWORD,
        is_adm: true,
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', null, {});
  }
};
```

Running this seed will create the first user that we can use in the project. Before we insert it in the database, let's input a password in the FIRST_USER_PASSWORD variable in the `.env` file.

The passwords stored are all encrypted, so we cannot use any password and place it directly in the `.env` file. We must encrypt it first. You can use the following encrypted password:
```
$2a$08$4Ep6My77GBmBA/t1m8NJjeuTImoFCdIYKEKxp1HrT8fGP0rmrizWa
```

Which holds the value: `myfirstpassword`

Once you place the encrpted password in the `.env` file, you can now seed your first user in the database. Run:
```bash
npx sequelize-cli db:seed:all
```

Which should output:
```bash
Sequelize CLI [Node: 8.11.4, CLI: 5.5.1, ORM: 5.21.5]

Loaded configuration file "src/config/database.js".
== 001-User: migrating =======
== 001-User: migrated (0.046s)
```

Now, kill the server and run `npm start` one last time.

If you followed all of the steps in this tutorial, your project should be ready to run smoothly from now on. Good Job!

p.s: There is a lib used in the project called `nodemon`, which detects changes to the project and re-runs the server every time you save a file. However, it cannot detect changes in hidden files such as the `.env` file, so every time you change something in there, you must manually kill the server and run `npm start` to run the project.
<br />

# Testing the login

If you want to quickly know if the project is really working, make a login request. It will go through every aspect of the project, including routing, code structure, password decrypting, database connection, and JWT generation, so if it works, you're good to go.

Make a POST /login request, with a JSON body including an e-mail and password fields. Use the user created in this tutorial if you haven't created a user yet.

If you have `curl` installed, run:
```bash
curl -d '{"email":"admin@ivet.com","senha":"myfirstpassword"}' -H 'Content-Type: application/json' http://localhost:8000/login
```

And you should get:
```bash
{
  "user": {
    "id": 2,
    "email": "admin@ivet.com"
  },
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNTg3MzA0MjA5LCJleHAiOjE1ODc0NzcwMDl9.m7Fm-PRfPC4i3_8uUD5aKXkaf50fSLRQF77O2JqCoUk"
}
```
<br />

# Insomnia

If you've ever used Postman, Insomnia's the same thing, but slicker. It's UX is really nice. You can use it to test all routes in the system. Download it @ https://insomnia.rest/download/

We've included a sample Insomnia workspace in the project, which already has all the project's routes and the JWT setup. Once you login through insomnia, it will automatically store the token from the response and use it in the requests that demand authentication. To import this workspace, open Insomnia, and go to `Application > Preferences > Data > Import Data > From File` and choose the json file in `assets/iVet_Insomnia.json`.